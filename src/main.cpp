#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <simgrid/s4u.hpp>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

#include "docopt/docopt.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(mwe, "Messages specific for this s4u MWE");




using namespace rapidjson;
using namespace std;
using namespace simgrid;

static void tcpkali_server();

vector<s4u::Host *> read_hostfile(std::string path) {
    vector<s4u::Host *> hosts;

    std::ifstream infile(path);

    std::string hostname;
    while (infile >> hostname)
    {
        hosts.push_back(simgrid::s4u::Host::by_name(hostname));
    }
    return hosts;
}


static void tcpkali_client(std::string server_host, double period_idle, double period_interf)
{
    XBT_INFO("Tcpkali client");
    auto e = s4u::Engine::get_instance();
    double t0 = e->get_clock();
    std::string mailbox_name = "mb_tcpkali";
    simgrid::s4u::Mailbox* mbox = simgrid::s4u::Mailbox::by_name(mailbox_name);
    // mbox->set_receiver(server);
    int *payload = new int;
    *payload = 42;

    while(true) {
        auto comm = mbox->put_async((void*) payload, 100e10);
        XBT_INFO("interference");
        simgrid::s4u::this_actor::sleep_for(period_interf);
        comm->cancel();
        XBT_INFO("idle");
        simgrid::s4u::this_actor::sleep_for(period_idle);
    }

    delete payload;
}

static void tcpkali_server()
{
    XBT_INFO("Tcpkali server");
    auto e = s4u::Engine::get_instance();
    double t0 = e->get_clock();
    std::string mailbox_name = "mb_tcpkali";
    void *data;

    while(true) {
        simgrid::s4u::Mailbox* mbox = simgrid::s4u::Mailbox::by_name(mailbox_name);
        auto comm = mbox->get_async(&data);
        try {
            comm->wait();
        } catch (simgrid::NetworkFailureException &e) {
            XBT_INFO("Stop interferences (this is normal)");
        }
    }
}

static void runner_seq()
{
    auto e = s4u::Engine::get_instance();
    double t0 = e->get_clock();
    simgrid::s4u::this_actor::execute(1e10);
    XBT_INFO("Actor %s on host %s having %d core. Took %g s", simgrid::s4u::this_actor::get_cname(),
             simgrid::s4u::this_actor::get_host()->get_cname(), simgrid::s4u::this_actor::get_host()->get_core_count(),
             e->get_clock() - t0);
}


/**
 * This function executes a parallel task from a json file and a hostfile.
 * The hostfile explicit the mapping between the ranks of the parallel task.
 * The json file holds the communication matrix, it should have the same format as a batsim profile.
 * ```
 * {
 *    "profile": {
 *      "com" : [ 0, 0, 0,
 *                0, 0, 0,
 *                0, 0, 0 ]
 *      "cpu" : [ 0, 0, 0 ]
 *    }
 * }
 * ```
 */
void start_parallel_task(std::string profile, std::string hostfile) {
    auto e = s4u::Engine::get_instance();

    FILE* fp = fopen(profile.c_str(), "rb"); // non-Windows use "r"
    char readBuffer[65536];
    FileReadStream is(fp, readBuffer, sizeof(readBuffer));
    Document d;
    d.ParseStream(is);
    fclose(fp);

    std::vector<double> comm;
    const Value& a = d["profile"]["com"];
    assert(a.IsArray());
    for (SizeType i = 0; i < a.Size(); i++) {
        auto amount = a[i].GetDouble();
        comm.push_back(amount);
    }

    std::vector<double> comp;
    const Value& comp_v = d["profile"]["cpu"];
    assert(a.IsArray());
    for (SizeType i = 0; i < comp_v.Size(); i++) {
        auto amount = comp_v[i].GetDouble();
        comp.push_back(amount);
    }

    auto hosts = read_hostfile(hostfile);

    simgrid::s4u::ExecPtr ptask = simgrid::s4u::this_actor::exec_init(hosts, comp, comm);
    XBT_INFO("Start parallel task");
    ptask->start();

    // simgrid::s4u::this_actor::sleep_for(1e-9);
    // simgrid::s4u::this_actor::parallel_execute(hosts, comp, comm);

    printf("time,progress\n");
    while(!ptask->test()) {
      simgrid::s4u::this_actor::sleep_for(1);
      auto clock = e->get_clock();
      auto rate  = ptask->get_remaining_ratio();
      printf("%f,%f\n", clock, 1 - rate);
    }
}

void start_simulation(map<string, docopt::value> args) {
  auto profile_file = args["--profile"].asString();
  auto hostfile = args["--hostfile"].asString();
  simgrid::s4u::ActorPtr cli, ser;

  auto ptask = simgrid::s4u::Actor::create("ptask",
      simgrid::s4u::Host::by_name("10.158.0.3"),
      start_parallel_task, profile_file, hostfile);

  if(args["interference"].asBool() == true) {
    XBT_INFO("Start tcpkali actors");
    auto idle_time = args["--idle"].asLong();
    auto interf_time = args["--interf"].asLong();

    std::string server = args["--server"].asString();
    std::string client = args["--client"].asString();

    cli = simgrid::s4u::Actor::create("tcpkali_client",
        simgrid::s4u::Host::by_name(client), // host
        tcpkali_client, // function
        server, idle_time, interf_time); // parameters

    ser = simgrid::s4u::Actor::create("tcpkali_server",
        simgrid::s4u::Host::by_name(server),
        tcpkali_server);

  }

  ptask->join();
  XBT_INFO("Parallel task finished");

  if(args["interference"].asBool() == true) {
    XBT_INFO("clean tcpkali actors");
    cli->kill();
    ser->kill();
    XBT_INFO("tcpkali killed");
  }
}


static const char USAGE[] =
R"(Simulation parameters.

    Usage:
      main <platform_file> <master_host> -p <profile> -H hostfile [--sg-cfg <opt_name:opt_value>...]
      main interference <platform_file> <master_host>
            -p <profile>
            -d <idle_time>
            -t <interf_time>
            -s <server>
            -c <client>
            -H hostfile
            [--sg-cfg <opt_name:opt_value>...]

    Options:
      -h --help                           Show this screen.
      --version                           Show version.
      -p, --profile <profile>             The json profile containing the parallel task inputs.
      -d, --idle <idle_time>              Idle time in the interference period.
      -t, --interf <interf_time>          Interference time in the interference period.
      -H, --hostfile <hostfile>           Hostfile mapping.
      -s, --server <server>               Host for tcpkali server.
      -c, --client <client>               Host for tcpkali client.
      --sg-cfg <opt_name:opt_value>       Forwards a given option_name:option_value to SimGrid.
                                          Refer to SimGrid configuring documentation for more information.
)";

int main(int argc, char* argv[])
{

    map<string, docopt::value> args = docopt::docopt(USAGE,
                                                    { argv + 1, argv + argc },
                                                     true,
                                                     "0.0.a");
//     for(auto const& arg : args) {
//       std::cout << arg.first << ":" <<  arg.second << std::endl;
//     }

    auto platform_file = args["<platform_file>"].asString();
    auto master_host = args["<master_host>"].asString();

    auto simgrid_config = args["--sg-cfg"].asStringList();

    // Instantiate SimGrid
    s4u::Engine engine(&argc, argv);
    // Setting SimGrid configuration options, if any
    for (const string & cfg_string : simgrid_config)
    {
      engine.set_config(cfg_string);
    }

    engine.set_config("host/model:ptask_L07");
    engine.load_platform(platform_file);

    simgrid::s4u::Actor::create("simulation",
        simgrid::s4u::Host::by_name(master_host), // host
        start_simulation, // function
        args); // parameters

    engine.run();
    XBT_INFO("Simulation done.");

    return 0;
}
