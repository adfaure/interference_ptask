{ kapack ? import
    # last working commit: f70b1149a9ae30fafe87af46e517985bacc331c3
    ( fetchTarball "https://github.com/oar-team/kapack/archive/master.tar.gz")
  {},
  pkgsMaster ? import (fetchTarball
  "https://github.com/NixOS/nixpkgs/archive/68801b40d60d2f760c925115591a0bd13c01467b.tar.gz")
  {}
}:

with kapack.pkgs;

let

  jupyter = import (builtins.fetchGit {
    url = https://github.com/tweag/jupyterWith;
    rev = "";
  }) {};

  #exptools = pkgs.callPackage ~/Projects/exptools {
  #};

  exptools = import (fetchTarball {
    url = https://gitlab.inria.fr/adfaure/ptasktools/-/archive/master/ptasktools-master.tar.gz;
  }) {
    inherit pkgs;
    pythonPackages = pkgs.python37Packages;
  };

  kaPython = jupyter.kernels.iPythonWith rec {
    name = "kapython";
    python3 = pkgs.python37;
    packages = p: with p; [
      ipython
      aiofiles
      pandas
      numpy
      ruamel_yaml
      matplotlib
      # python37Packages.ortools
      exptools
      # rpy2 + deps
      tzlocal
      simplegeneric
      wheel
      (rpy2.overrideAttrs(attrs : {
        buildInputs = attrs.buildInputs ++ (with pkgsMaster.rPackages; [ tidyverse viridis ]);
      }))
    ];
  };

  jupyterEnvironment =
    jupyter.jupyterlabWith {
      kernels = [
        kaPython
      ];
  };

  self = rec {
    experiment_env = mkShell rec {
      name = "experiment_env";
      buildInputs = [
        (pkgs.rstudioWrapper.override {
          packages = with rPackages; [tidyverse viridis];
        })
        # simulator
        kapack.simgrid
        kapack.docopt_cpp
        boost
        meson
        ninja
        rapidjson
        pkgconfig
        # Data analysis
        jupyterEnvironment
      ];
    };
  };
in
  self.experiment_env
